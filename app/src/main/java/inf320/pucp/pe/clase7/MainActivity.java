package inf320.pucp.pe.clase7;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;


public class MainActivity extends Activity {
    private static File foto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: llamar a la cámara
                Intent tomaFoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                foto = new File(Environment.getExternalStorageDirectory(), "clase7.jpg");
                tomaFoto.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(foto));

                if (tomaFoto.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(tomaFoto, 320);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 320 && resultCode == RESULT_OK) {
            //Bundle extras = data.getExtras();
            //Bitmap imageBitmap = (Bitmap) extras.get("data");
            ImageView imageView = (ImageView) findViewById(R.id.imageView);
            //imageView.setImageBitmap(imageBitmap);

            //imageView.setImageBitmap(BitmapFactory.decodeFile(foto.getPath()));

            Bitmap bmpFoto = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(foto.getPath()), 800, 600, true);
            imageView.setImageBitmap(bmpFoto);
        }
    }

    private void detectaEdad(final Bitmap imageBitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 80, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        AsyncTask<InputStream, String, Face[]> detecta =
                new AsyncTask<InputStream, String, Face[]>() {
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0], false, false, false, false);
                            if (result == null)
                            {
                                publishProgress("Detection Finished. Nothing detected");
                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected",
                                            result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");
                            return null;
                        }
                    }
                    @Override
                    protected void onPreExecute() {
                        //TODO: show progress dialog

                    }
                    @Override
                    protected void onProgressUpdate(String... progress) {
                        //TODO: update progress

                    }
                    @Override
                    protected void onPostExecute(Face[] result) {
                        //TODO: update face frames

                    }
                };
        detecta.execute(inputStream);
    }

}
